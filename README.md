# Daily task list generator plugin

Joplin plugin to automatically generate a daily todo list based on due 
dates and priority tags.
