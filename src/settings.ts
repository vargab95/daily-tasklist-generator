import joplin from "../api";
import { SettingItemType } from "../api/types";

const settingsSectionLabel = "dailyTaskListGeneratorSettingsSection";

export async function registerSettings(): Promise<void> {
  await joplin.settings.registerSection(settingsSectionLabel, {
    label: "Daily task list generator",
  });
  await joplin.settings.registerSettings({
    maximumNumberOfTodosInDailyTaskList: {
      value: 10,
      minimum: 1,
      maximum: 1024,
      type: SettingItemType.Int,
      section: settingsSectionLabel,
      public: true,
      label: "Maximum number of todos in a daily task list",
    },
    dailyTaskListTodoFilterStrings: {
      value: [
        "due:19700201 -due:day+1",
        "tag:asap -due:day+1",
        'tag:"high priority" -due:day+1',
        'tag:"mid priority" -due:day+1',
        'tag:"low priority" -due:day+1',
      ],
      type: SettingItemType.Array,
      section: settingsSectionLabel,
      public: true,
      label: "Filter strings for getting todos",
      description:
        "The filters should be ordered by priority. First has the highest priority.",
    },
  });
}

export function setFilters(filters: string[]): Promise<void> {
  return joplin.settings.setValue("dailyTaskListTodoFilterStrings", filters);
}

export function getFilters(): Promise<string[]> {
  return joplin.settings.value("dailyTaskListTodoFilterStrings");
}
