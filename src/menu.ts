import joplin from "../api";
import { MenuItemLocation } from "../api/types";
import { CommandNames } from "./commands";
import { getFilters, setFilters } from "./settings";

let dialog = null;

export async function setupFilterDialog() {
  dialog = await joplin.views.dialogs.create(
    "dailyTaskListGeneratorFiltersDialog"
  );
}

const escapeRules = [
  { expression: /&/g, replacement: "&amp;" },
  { expression: /</g, replacement: "&lt;" },
  { expression: />/g, replacement: "&gt;" },
  { expression: /"/g, replacement: "&quot;" },
  { expression: /'/g, replacement: "&#039;" },
];

function escapeFilter(filter: string): string {
  let result = filter;
  for (const rule of escapeRules) {
    result = result.replace(rule.expression, rule.replacement);
  }

  return result;
}

export async function openFilterDialog() {
  let html = `
    <form id="filtersStringForm" name="filtersStringForm">
      <fieldset id="filtersFieldSet">
        <legend>Daily task list filters</legend>
  `;
  const filters = await getFilters();

  for (const [index, filter] of filters.entries()) {
    html += `
      <div>
        <input id="filter-${index}" name="filter-${index}" value="${escapeFilter(
      filter
    )}">
      <button type="button" onclick="removeInput(this)">x</button>
      </div>
    `;
  }

  html += `
      </fieldset>
      <button type="button" onclick="addInput()">+</button>
    </form>
  `;

  // const pluginPath = await joplin.plugins.installationDir();
  await joplin.views.dialogs.setHtml(dialog, html);
  await joplin.views.dialogs.addScript(dialog, "/ui/filterSettings.js");
  await joplin.views.dialogs.addScript(dialog, "/ui/filterSettings.css");
  const formResult = await joplin.views.dialogs.open(dialog);
  if (formResult.id == "ok") {
    console.log(formResult.formData.filtersStringForm);
    setFilters(Object.values(formResult.formData.filtersStringForm));
  }
}

export async function createMenu(commandNames: CommandNames): Promise<void> {
  await joplin.views.menus.create(
    "generateDailyTaskListMenuItem",
    "Daily task list generator",
    [
      { commandName: commandNames.generate, accelerator: "Ctrl+Shift+D" },
      { commandName: commandNames.configure },
    ],
    MenuItemLocation.Tools
  );
}
