import joplin from "../api";
import { openFilterDialog } from "./menu";

type Note = { id: string; title: string; todo_due: number };

export type CommandNames = {
  generate: string;
  configure: string;
};

const commandNames = {
  generate: "generateDailyTaskList",
  configure: "configureDailyTaskListFilters"
};

function getFormattedDate(): string {
  const currentDate = new Date();

  const day = currentDate.getDate();
  const month = currentDate.getMonth() + 1;
  const year = currentDate.getFullYear();

  const formattedDay = day.toString().padStart(2, "0");
  const formattedMonth = month.toString().padStart(2, "0");

  return `${formattedDay}/${formattedMonth}/${year}`;
}

function removeDuplicates(arr: Note[]): Note[] {
  const uniqueMap = new Map<string, Note>();

  arr.forEach((item) => {
    const keyValue = item.id;
    if (!uniqueMap.has(keyValue)) {
      uniqueMap.set(keyValue, item);
    }
  });

  return Array.from(uniqueMap.values());
}

export async function registerCommand(): Promise<CommandNames> {
  await joplin.commands.register({
    name: commandNames.generate,
    label: "Generate daily task list",
    execute: async () => {
      const maxNumberOfTodos: number = await joplin.settings.value(
        "maximumNumberOfTodosInDailyTaskList",
      );
      const filters: string[] = await joplin.settings.value(
        "dailyTaskListTodoFilterStrings",
      );
      let todos: Note[] = [];
      for (const filter of filters) {
        let pageNum = 0;
        let response: {
          items: Note[];
          has_more: boolean;
        };
        do {
          response = await joplin.data.get(["search"], {
            query: `type:todo iscompleted:0 ${filter}`,
            fields: ["id", "title", "todo_due"],
            type: "note",
            order_by: "todo_due",
            order_dir: "ASC",
            page: pageNum++,
          });
          todos = removeDuplicates(todos.concat(response.items));
          if (todos.length > maxNumberOfTodos) {
            todos.splice(maxNumberOfTodos);
            break;
          }
        } while (response.has_more);
      }

      console.log(todos);

      const heading = `# ${getFormattedDate()}\n\n`;
      const todoList = todos
        .map((todo) => `- [ ] [${todo.title}](:/${todo.id})`)
        .join("\n");

      if (todos.length > 0) {
        const note = await joplin.workspace.selectedNote();
        await joplin.data.put(["notes", note.id], null, {
          body: heading + todoList + "\n\n" + note.body,
        });
      } else {
        joplin.views.dialogs.showMessageBox("You've completed all todos");
      }
    },
  });

  await joplin.commands.register({
    name: commandNames.configure,
    label: "Configure daily task list filters",
    execute: async () => {
      await openFilterDialog();
    },
  });

  return commandNames;
}
