import joplin from "api";
import { registerCommand } from "./commands";
import { createMenu, setupFilterDialog } from "./menu";
import { registerSettings } from "./settings";


joplin.plugins.register({
  onStart: async function () {
    await setupFilterDialog();
    await registerSettings();
    const commandName = await registerCommand();
    await createMenu(commandName);
  },
});
