function removeInput(element) {
  element.parentElement.remove();
}

function addInput() {
  const inputs = document.querySelectorAll("fieldset#filtersFieldSet > div > input");
  const fieldSet = document.getElementById("filtersFieldSet");
  let newFilterId = 0;

  for (const input of inputs) {
    const inputId = input.id.split("-")[1];
    if (inputId > newFilterId) {
      newFilterId = inputId;
    }
  }
  newFilterId++;

  const div = document.createElement("div");
  const input = document.createElement("input");
  input.setAttribute("id", "filter-" + newFilterId.toString());
  input.setAttribute("name", "filter-" + newFilterId.toString());
  input.setAttribute("value", "");
  div.appendChild(input);

  const button = document.createElement("button");
  button.innerHTML = "x";
  button.onclick = () => removeInput(button);
  div.appendChild(button);

  fieldSet.appendChild(div);
}
